// @ts-ignore
import Dice1 from './assets/img/dice-1.png';
// @ts-ignore
import Dice2 from './assets/img/dice-2.png';
// @ts-ignore
import Dice3 from './assets/img/dice-3.png';
// @ts-ignore
import Dice4 from './assets/img/dice-4.png';
// @ts-ignore
import Dice5 from './assets/img/dice-5.png';
// @ts-ignore
import Dice6 from './assets/img/dice-6.png';

// Selectors
const player0El = document.querySelector('.player--0')!;
const player1El = document.querySelector('.player--1')!;
const score0El = document.getElementById('score--0')!;
const score1El = document.getElementById('score--1')!;
const current0El = document.getElementById('current--0')!;
const current1El = document.getElementById('current--1')!;

const diceEl = document.querySelector('.dice')!;
const btnNew = document.querySelector('.btn--new')!;
const btnRoll = document.querySelector('.btn--roll')!;
const btnHold = document.querySelector('.btn--hold')!;

let scores: number[],
  currentScore: number,
  activePlayer: number,
  playing: boolean,
  rolling: boolean,
  currentClass: string;

// Functions
const init = () => {
  scores = [0, 0];
  currentScore = 0;
  activePlayer = 0;
  currentClass = '';
  playing = true;
  rolling = false;

  score0El.textContent = '0';
  score1El.textContent = '0';
  current0El.textContent = '0';
  current1El.textContent = '0';

  diceEl.classList.add('hidden');
  player0El.classList.remove('player--winner');
  player1El.classList.remove('player--winner');
  player0El.classList.add('player--active');
  player1El.classList.remove('player--active');
};
init();

const rollDice = (dice: number) => {
  let showClass = `show-${dice}`;
  if (currentClass) {
    diceEl.classList.remove(currentClass);
  }
  diceEl.classList.add(showClass);
  currentClass = showClass;

  setTimeout(() => {
    // 3. Check for rolled 1: if true
    if (dice !== 1) {
      // Add dice to current score
      currentScore += dice;
      const currPlayer = document.getElementById(`current--${activePlayer}`)!;
      currPlayer.textContent = currentScore.toString();
    } else {
      // Switch to next player
      switchPlayer();
    }
    rolling = false;
    btnRoll.textContent = '🎲 Roll dice';
  }, 1400);
};

const switchPlayer = () => {
  const currPlayer = document.getElementById(`current--${activePlayer}`)!;
  currPlayer.textContent = '0';
  currentScore = 0;
  activePlayer = activePlayer === 0 ? 1 : 0;
  player0El.classList.toggle('player--active');
  player1El.classList.toggle('player--active');
};

// Game Logic
btnRoll.addEventListener('click', function() {
  if (playing && !rolling) {
    rolling = true;
    btnRoll.textContent = '🎲 Rolling!';
    // 1. Generating a random dice roll
    diceEl.classList.remove();
    let dice = Math.trunc(Math.random() * 6) + 1;
    if (diceEl.classList.contains(`show-${dice}`)) {
      if (dice === 6) {
        dice--;
      } else {
        dice++;
      }
    }
    // 2. Display dice
    diceEl.classList.remove('hidden');
    rollDice(dice);
  }
});

btnHold.addEventListener('click', function() {
  if (playing && !rolling) {
    // 1. Add current score to active player's score
    scores[activePlayer] += currentScore;
    const currPlayerScoreEl = document.getElementById(`score--${activePlayer}`)!;
    currPlayerScoreEl.textContent = scores[activePlayer].toString();

    // 2. Check if player's score is >= 100
    if (scores[activePlayer] >= 100) {
      // Finish the game
      playing = false;
      diceEl.classList.add('hidden');
      const currPlayer = document.querySelector(`.player--${activePlayer}`)!;
      currPlayer.classList.add('player--winner');
      currPlayer.classList.remove('player--active');
    } else {
      // Switch to the next player
      switchPlayer();
    }
  }
});

btnNew.addEventListener('click', init);